## System Status API

## Init log
List of the init lines displayed on the startup screen.
```
POST server/init_logs
```

Resolution response
```json
POST server/init_logs

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.support.nanohfs.ContentDeliveryService"]}}
```

Example response
```json
{"success":true,"results":{"data":[{"tag":"CalibrateClock","status":"checking","result":"","log":""},
...
}
```

## Handshake values
List values stored in the handshake settings database used by all apps on the system.
```
POST server/handshake
```

Resolution response
```json
POST server/handshake

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.support.nanohfs.ContentDeliveryService"]}}
```

Example response
```json
{
{"success":true,"results":{"data":[
{"name":"GMTOffset","value":"-800"},
{"name":"Name","value":"NTN Site Hub - Test Site #2"},
{"name":"RouterSSID","value":"Buzztime161594909190"},
{"name":"SiteHub.Settings.AvatarUrl","value":"https:\/\/dev.buzztime.com\/images\/avatardrop\/png\/%s.png"},
{"name":"SiteHub.Settings.NightlyRebootDisabled","value":"false"},
{"name":"SiteHub.Settings.Test3rdPartyConfig","value":"3rdpartyHello"},
{"name":"SiteHub.Settings.allow_core_updates","value":"true"},
...
]}}
```


## Confidential keys.
List the confidential keys stored on the system by name and digest. The actual data is not provided, but the digest can be used to match the data to the confidential data stored in the backend db.
```
POST server/confidential_keys
```
Resolution response
```json
POST server/confidential_keys

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.support.nanohfs.ContentDeliveryService"]}}
```
Example response
```json
{
POST server/confidential_keys

{"success":true,"results":{"data":[
{"name":"SiteHub.SiteManifest.token","etag":"bc5a014a36d69b68c7881f64098a6347"},
{"name":"SiteHub.mqttbroker.password","etag":"8ad41d126ceee215bcd5f83df78539cd"},
{"name":"SiteHub.mqttbroker.username","etag":"5fa212eff15e331f544be288015e5c32"},
{"name":"SiteHub.platform.secret","etag":"e55c313b5b988049a59aac89ed98dc53"},
{"name":"SiteHub.shared.secret","etag":"704571a993a2b36a69490672024feadd"},
{"name":"SiteHub.wifiap.secret","etag":"9dba36993fa2c26cb2aa335b44856030"},
{"name":"nexus.librarian","etag":"105edd69f8e4f406e453d3229f85aa6f"},
{"name":"nexus.repoman","etag":"a1c8218e4b2da7ab7ca417de4f23687d"}
]}}
```


## Site Control
List of actions that can be invoked on the device .
```
POST server/site_ops
```

Resolution response
```json
POST server/site_ops

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.support.nanohfs.ContentDeliveryService"]}}
```

Example response
```json
POST server/site_ops

{"success":true,"results":{"data":[
{"group":"Systems Control","members":[
{"name":"Reboot","subject":"Sitehub","target":"server","action":"reboot_sitehub"},
{"name":"Reboot","subject":"WAP","target":"server","action":"reboot_wap"}
]},
{"group":"System Updates","members":[
{"name":"Update","subject":"Core App","target":"core_app","action":"update"}
]},
{"group":"Operations","members":[
{"name":"Logcat","subject":"Sitehub","target":"core_app","action":"logcat"}
]}
]}}
```

## Site Info
Properties of the device.
```
POST server/site_info
```

Resolution response
```json
POST server/site_info

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.support.nanohfs.ContentDeliveryService"]}}
```

Example response
```json
POST server/site_info

{"success":true,"results":
{"DataFreeBytes":"12540964864",
"DataTotalBytes":"13776191488",
"DeviceIdentifier":"4b2081ea216d6b08",
"FirmwareDisplay":"bzt232hub01-userdebug 7.1.2 NHG47K 103 test-keys",
"FirmwareManufacturer":"buzztime",
"FirmwareModel":"BZT-S101",
"FirmwareOSVersion":"7.1.2",
"FirmwareSDKVersion":"25",
"SerialNumber":"180528188002103",
"SiteIPAddress":"172.23.59.80",
"SoftwareVersion":"1.00",
"WAPModelFirmware":"unknown"
}}
```