## Content API
* [Package Logtags](package-logs.md)
* [System Logs](site-logs.md)
* [System Updates](system-updates.md)

### Media Content
Listing of all the content files downloaded by the MediaContentProvider as determined by the current content manifest
```
POST server/content_files
```

Resolution response
```json
POST server/content_files

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.support.nanohfs.ContentDeliveryService"]}}
```

Example response
```json

{"success":true,"results":{"data":[
{"fileId":242649,"fileVersionId":333,"remoteURL":"http:\/\/cdn.dev.buzztime.com\/0007AC\/tablet\/themes\/BTTheme_40727_333.zip?ec_rate=20","contentPath":"\/tablet\/themes\/BTTheme_40727.zip","md5":"b760d7f87148a14a04d5a8f096c006e1","downloadComplete":1},
{"fileId":242713,"fileVersionId":402,"remoteURL":"http:\/\/cdn.dev.buzztime.com\/0007AC\/tablet\/btfoodmenu\/images\/BTFoodMenu_Images_EF_402.zip?ec_rate=20","contentPath":"\/tablet\/btfoodmenu\/images\/BTFoodMenu_Images_EF.zip","md5":"8f8edc75a1d78d394b79c4e8495162e5","downloadComplete":1},
{"fileId":242717,"fileVersionId":407,"remoteURL":"http:\/\/cdn.dev.buzztime.com\/0007AC\/tablet\/btfoodmenu\/images\/BTFoodMenu_Images_BW_407.zip?ec_rate=20","contentPath":"\/tablet\/btfoodmenu\/images\/BTFoodMenu_Images_BW.zip","md5":"23f626d893515f21a22b1cd7dfb374ae","downloadComplete":1},
...
]}}

```


### Installed Packages
List all the packages that are not preinstalled on the firmware
```
POST server/installed_apks
```

Resolution response
```json
POST server/installed_apks

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.support.nanohfs.ContentDeliveryService"]}}
```

Example response
```json
POST server/installed_apks

{"success":true,"results":{"data":[
 {"package":"com.buzztime.core.rabcon","version":"2.0.1-SNAPSHOT","build":74,"installer":"manifest"},
 {"package":"com.buzztime.core.CoreApp","version":"1.6.1-SNAPSHOT","build":135,"installer":"manifest"},
 {"package":"com.buzztime.sched.TriviaEngine.channel1","version":"1.0.0-SNAPSHOT","build":81,"installer":"manifest"},
 {"package":"com.buzztime.sched.triviaspectator","version":"1.0.0-SNAPSHOT","build":82,"installer":"manifest"},
 ...
 ]}}

```


