## System Update Panel

### Overview
Firmware updates are in a beta stage currently.  The goal at this point is to identify the problems that may occur during an automated firmware upgrade.  The process has been broken down into steps that can be assessed indiviually for success and failure. 

Provide the url to a firmware manifest.  Diff will provide an assessment of the firmware. If applicable the Download block will appear.  Once downloaded, the Stage block will appear.  'Stage' will validate the chcksums and extract the compressed image.  Once staged, the firmware image is ready to install.  'Proceed' will begin the system update.

The process is managed with a single json report that documents the state flow through the process.  The json data is organized into blocks representing the stages and data results:

Block | scope | purpose
----- | ------| --------------
"installed"  | always | contains details about the currently installed firmware image
"manifest" | manifest | information needed to request the manifest data
"target" | manifest | documents the manifest data
"diff" | manifest | once a manifest is provided, the diff section will contain comparisons to the installed image
"download" | always | once a download is triggered, this section will contain the download id and progress
"stage" || contains data on three sequential processes, Check, Extract, Verify.  Check will validate the checksum for the zipped image, then the image is extracted and finally the checksum for written uncompressed image is validated.
"install" || final stage that will again verify the checksum and then trigger the OS to begin the system update

### Flow
The manifest will contain the url to the compressed firmware image.   
Platform note: The firmware build process contains a bash script (mkpublish.sh) to generate the manifest info, compress the firmware image and upload both to the nexus repo.  The script will be called after mkupdate.sh.
Once published on Nexus, one can find the firmware manifest and copy the url and paste it into the Dash app ( Mystery Machine when avail) to initiate the first step for a System Update.

### Steps for System Update on a device:
1. Submit a manifest url to the device.  
Locate the System Update panel on the device Dash app.  Paste the url for the manifest into the Firmware: field.  
The product and version info will be compared to the device product and version info.  The build.product and build.model must match exactly.  The build.type SHOULD match. The build.number may vary in the final 2 numbers but it must match in the major version.  The build.id SHOULD be different.  
Acceptable url manifests will enable the Download section’s GO action.
1. Trigger the device to download the compressed firmware image  
Wait for the device to finish downloading the compressed image file.
1. Once the download is completed, refresh the panel to enable
1. Stage the downloaded firmware image.
   - Match the SHA256 digest of the downloaded image to the calculated digest
   - Extract the uncompressed file
   - Match the calculated SHA256 digest for the uncompressed file
   - Rename the file to _update.img in prep for starting the install
1.Start the install
   - Match the SHA256 digest of the uncompressed _update.img file as it was written.  This redundant check verifies that the file was correctly written to disk.
   - Rename the _update.img file to update.img
1. Trigger the System level update process.
By design, steps 1-3 can be issued and cleared.  Clearing a step will clear any results for that step and the following steps.  Step 4 is not cancelable once it has been triggered.


![Panel](../assets/stage.png)


### Initiating and clearing stages
Stage are activated and cleared by posting data along with the server/system_update call

key | value 
--- | ------
start | the stage to begin, one of 'target', 'download','stage','install'
clear | the stage to be cleared, one of 'target','download','stage','install'

Example 
```
replyto session/Ka0HOEDYRiwe4rbx
POST server/system_update

{ 'start': 'stage' }
```

The call will initiate or clear the specified stage and then return the full system_update body to the replyto subject







