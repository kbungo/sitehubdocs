### Package Log Editor
List out declared log.tag entries from applications
```
POST server/pkg_loglevel
```

Confirmation response
```json
POST server/pkg_loglevel

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.support.nanohfs.ContentDeliveryService"]}}
```

Response
```json
POST server/pkg_loglevel

{"success":true,"results":{"data":[
  {"group":"com.buzztime.core.ContentManagement",
   "members":[
   {"name":"log.tag.ContentCleanupService","value":"DEBUG"}
   ]},
  {"group":"com.buzztime.core.CoreApp",
   "members":[
   {"name":"log.tag.BootupActivity","value":"DEBUG"},
   {"name":"log.tag.BtNexusSW_Upd","value":"DEBUG"},
   {"name":"log.tag.CalibrateClock","value":"DEBUG"},
   {"name":"log.tag.DashOpsService","value":"DEBUG"},
   {"name":"log.tag.DeploymentService","value":"DEBUG"},
   {"name":"log.tag.JWTAuthorization","value":"DEBUG"},
   {"name":"log.tag.PlatformStartupCklst","value":"DEBUG"},
   {"name":"log.tag.SecureUtils","value":"DEBUG"},
   {"name":"log.tag.Volley","value":"DEBUG"},
   {"name":"log.tag.coreApp","value":"DEBUG"}
   ]}
  ]}
}
```
#### Setting logtags
Commands are issued by specifying the target/action in the POST line

 name | value
 ------- | -------
 method | GET 
 get | name value pairs
 name | name of the log.tag. to be modified or a full package 
 value | one of SUPPRESS,ERROR,WARN,INFO,DEBUG,VERBOSE

Example Request
```
replyto sitehub/40882
POST logtag/pkg_loglevel

{
	"method": "GET",
	"get": {
		"name": "log.tag.Supervisor.OSD",
		"value": "DEBUG"
	}
}
```

#### Setting all logtags for a package
 
Example Request
```
replyto sitehub/40882
POST logtag/pkg_loglevel

{"method":"GET","get":{"name":"com.buzztime.sched.Supervisor","value":"SUPPRESS"}}
```

Resolution response
```
POST logtag/pkg_loglevel

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.core.dashops.DashOpsService"]}}
```

